package com.droidappster.speed;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.droidappster.speed.com.droidappster.speed.utility.Constants;
import com.droidappster.speed.com.droidappster.speed.utility.NotificationHelper;
import com.droidappster.speed.com.droidappster.speed.utility.SpeedHistoryDB;
import com.droidappster.speed.com.droidappster.speed.utility.WearEventListener;
import com.droidappster.speed.com.droidappster.speed.utility.WearHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocationService extends Service  implements LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, WearEventListener{
    public static final int LOCATION_REQUEST_INTERVAL = 200;
    /** MPS: Meter per second, KMPH: KiloMeter per hour, MPH: Miles per hour */
    public static final float MPS_TO_MPH = 2.2369362920544025f;
    public static final float MPS_TO_KMPH = 3.6f;

    public static final String ACTION_START = "speed/START";
    public static final String ACTION_STOP = "speed/STOP";
    public static final String ACTION_PAUSE = "speed/PAUSE";
    public static final String ACTION_RESET = "speed/RESET";
    public static final String ACTION_GET_STATUS = "get/STATUS";

    private static final String TAG = "LocationService";
    private LocationRequest locationRequest;
    private LocationClient mLocationClient;
    private final Context context = this;
    private LocalBinder binder;
    private List<SpeedListener> locationListeners;
    private float currentMaxSpeed = 0;
    private float currentAvgSpeed = 0;
    private long count = 0;
    private boolean mIsMetric = false;
//    private WearHelper wearHelper;
    private boolean sendWearData = false;
    private ArrayList<Float> speeds = new ArrayList<Float>(5);

    public static enum State{
        RUNNING,
        PAUSED,
        STOPPED,
        NONE
    }

    public LocationService() {
//        wearHelper = new WearHelper(this);
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        mIsMetric = prefs.getBoolean(Constants.PREFS_SPEED_UNIT, false);
        locationListeners = Collections.synchronizedList(new ArrayList<SpeedListener>());
        binder = new LocalBinder();
//        wearHelper.onCreate(context);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
//            wearHelper.onStart();
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals(ACTION_START)) {
                    start();
                } else if (intent.getAction().equals(ACTION_PAUSE)) {
                    pause();
                } else if (intent.getAction().equals(ACTION_STOP)) {
                    stop();
                } else if (intent.getAction().equals(ACTION_RESET)) {
                    stop();
                    start();
                } else if(intent.getAction().equals(ACTION_GET_STATUS)){
//                    sendStateChangeToWear(State.NONE);
//                    sendStateChangeToWear(state);
                }
            }
        } catch(Exception e){}
        return Service.START_STICKY;
    }

    public boolean isMetric(){
        return mIsMetric;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
//        wearHelper.onStop();
        Log.d("LocationService", "onDestroy()");
        super.onDestroy();
    }

    public void registerListener(SpeedListener listener){
        if(listener==null){throw new IllegalArgumentException("listener cannot be null");}

        if(!locationListeners.contains(listener)){
            locationListeners.add(listener);
        }
    }

    @Override
    public void connected(Bundle connectionHint) {
        sendWearData = true;
    }

    private DecimalFormat df = new DecimalFormat(Constants.SPEED_FORMATTER);
//    public void sendSpeedToWear(){
//        PutDataMapRequest dataMap = PutDataMapRequest.create("/speed");
//        dataMap.getDataMap().putString("speed", df.format(getSpeed()));
//        dataMap.getDataMap().putString("max", df.format(getMaxSpeed()));
//        dataMap.getDataMap().putBoolean("metric", mIsMetric);
//
//        sendDataMapToWear(dataMap);
//    }

//    private void sendStateChangeToWear(State state){
//        PutDataMapRequest dataMap = PutDataMapRequest.create("/speed/state");
//        dataMap.getDataMap().putString("state", state.name());
//        sendDataMapToWear(dataMap);
//    }
//
//    private void sendDataMapToWear(PutDataMapRequest dataMap) {
//        PutDataRequest request = dataMap.asPutDataRequest();
//        PendingResult<DataApi.DataItemResult> pendingResult = Wearable.DataApi
//                .putDataItem(wearHelper.getGoogleApiClient(), request);
//        pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
//            @Override
//            public void onResult(DataApi.DataItemResult dataItemResult) {
//            }
//        });
//    }

    private volatile State state = State.STOPPED;
    public boolean isRunning(){
        return state.equals(State.RUNNING);
    }

    public void setMetric(boolean isMetric){
        this.mIsMetric = isMetric;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(Constants.PREFS_SPEED_UNIT, isMetric);
        editor.commit();
        for(SpeedListener listener : locationListeners){
            listener.onUnitChanged(isMetric);
        }
    }

    private boolean shouldRun = false;
    private long duration = 0;
    public State start(){
        if(!state.equals(State.RUNNING)) {
            if (mLocationClient != null && mLocationClient.isConnected()) {
                mLocationClient.requestLocationUpdates(locationRequest, this);
//                wearHelper.sendMessage(State.RUNNING);
                if (!state.equals(State.PAUSED)) {
                    currentAvgSpeed = 0;
                    currentMaxSpeed = 0;
                    for(int i=0;i<speeds.size();i++) {
                        speeds.set(i, 0.0f);
                    }
                    duration = System.currentTimeMillis();
                }
                state = State.RUNNING;
                NotificationHelper.showNotification(context, "Tracking speed...", "Be safe!", state);
                for (SpeedListener listener : locationListeners) {
                    listener.onSpeedTrackingStarted();
                }
//                sendStateChangeToWear(state);
            } else{

                mLocationClient = new LocationClient(context, this, this);
                locationRequest = LocationRequest.create();
                locationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
                locationRequest.setFastestInterval(100);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                mLocationClient.connect();
                shouldRun = true;
            }
        }
        return state;
    }

    public void pause(){
        state = State.PAUSED;
//        wearHelper.sendMessage(State.PAUSED);
//        sendStateChangeToWear(state);
        NotificationHelper.showNotification(context, "Speed tracker paused!", "Take your time...", state);
        mLocationClient.removeLocationUpdates(this);
        for(SpeedListener listener : locationListeners){
            listener.onSpeedTrackingPaused();
        }
    }

    public void stop(){
        duration = System.currentTimeMillis() - duration;
        if(currentAvgSpeed>0 || currentMaxSpeed>0){
            SpeedHistoryDB.getInstance(context).insert(currentMaxSpeed, currentAvgSpeed, duration);
        }
        state = State.STOPPED;
//        wearHelper.sendMessage(State.STOPPED);
//        sendStateChangeToWear(state);
        if(mLocationClient !=null) {
            mLocationClient.removeLocationUpdates(this);
        }
        count = 0;
        tempCount = 0;
        NotificationHelper.clearNotification(context);
        for(SpeedListener listener : locationListeners){
            listener.onSpeedTrackingStopped();
        }
    }

    public void unregisterListener(SpeedListener listener){
        for(int i=0;i<locationListeners.size();i++){
            locationListeners.remove(listener);
        }
    }

    public float getMaxSpeed(){
        if(mIsMetric) {
            return currentMaxSpeed * MPS_TO_KMPH;
        } else{
            return currentMaxSpeed * MPS_TO_MPH;
        }
    }

    public float getAverageSpeed(){
        if(mIsMetric) {
            return currentAvgSpeed * MPS_TO_KMPH;
        } else{
            return currentAvgSpeed * MPS_TO_MPH;
        }
    }

    public float getSpeed() {
        if(mIsMetric) {
            return speeds.get(2) * MPS_TO_KMPH;
        } else{
            return speeds.get(2) * MPS_TO_MPH;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");
        if(shouldRun){
            start();
            shouldRun = false;
        }
    }

    @Override
    public void onDisconnected() {
        Log.d(TAG, "onDisconnected");
    }

    private short tempCount = 0;
    @Override
    public void onLocationChanged(Location location) {
        System.out.print("Speed: "+location.getSpeed());
        speeds.add(location.getSpeed());
        ArrayList<Float> tempArray = new ArrayList<Float>(speeds);
        Collections.sort(tempArray);
        if(speeds.size()>5) {
            speeds.remove(0);
            float speed = tempArray.get(2);
            if(tempCount>5) {
                currentAvgSpeed = ((currentAvgSpeed * count) + speed) / ++count;
                currentMaxSpeed = Math.max(currentMaxSpeed, speed);
            } else {
                tempCount++;
            }
//            if(sendWearData) {
//                sendSpeedToWear();
//            }

            for(SpeedListener listener : locationListeners){
                listener.onSpeedUpdated(location);
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
        for(SpeedListener listener : locationListeners){
            listener.onConnectionFailed(connectionResult);
        }
    }

    public class LocalBinder extends Binder{
        LocationService getService(){
            return LocationService.this;
        }
    }
}
