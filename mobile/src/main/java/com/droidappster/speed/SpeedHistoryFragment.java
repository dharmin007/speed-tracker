package com.droidappster.speed;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.droidappster.speed.com.droidappster.speed.utility.AsyncSpeedLoader;
import com.droidappster.speed.com.droidappster.speed.utility.Constants;
import com.droidappster.speed.com.droidappster.speed.utility.SpeedHistoryDB;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class SpeedHistoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<SpeedItem>>{

    private SpeedHistoryAdapter speedAdapter = null;

    private TextView overlayMaxSpeed;
    private TextView overlayAvgSpeed;
    private TextView overlayDuration;
    private TextView overlayDateTime;
    private View detailsOverlay;
    private Button deleteButton;
    private boolean isMetric;

    private static final DecimalFormat df = new DecimalFormat(Constants.SPEED_FORMATTER);

    public SpeedHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historyitem_grid, null);
        speedAdapter = new SpeedHistoryAdapter(getActivity(), Collections.EMPTY_LIST);
        GridView gridView = (GridView)view.findViewById(android.R.id.list);
        gridView.setEmptyView(view.findViewById(android.R.id.empty));
        gridView.setAdapter(speedAdapter);
        gridView.setOnItemClickListener(onItemClickListener);

        overlayAvgSpeed = (TextView) view.findViewById(R.id.txtAvgSpeed);
        overlayMaxSpeed = (TextView) view.findViewById(R.id.txtMaxSpeed);
        overlayDuration = (TextView) view.findViewById(R.id.txtDuration);
        overlayDateTime = (TextView) view.findViewById(R.id.txtDateTime);
        deleteButton = (Button) view.findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(onDeleteClickListener);
        detailsOverlay = view.findViewById(R.id.details_overlay);
        detailsOverlay.setOnClickListener(mSetDetailsOverlayClickListener);

        isMetric = PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(Constants.PREFS_SPEED_UNIT, false);
        return view;
    }

    private final AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            fillDetailsOverlay(position);
            detailsOverlay.setVisibility(View.VISIBLE);
        }
    };

    private final View.OnClickListener onDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SpeedHistoryDB.getInstance(getActivity()).delete((Integer) v.getTag());
            detailsOverlay.setVisibility(View.GONE);
            getLoaderManager().restartLoader(0, null, SpeedHistoryFragment.this);
        }
    };

    private void fillDetailsOverlay(int position) {
        SpeedItem item = (SpeedItem) speedAdapter.getItem(position);

        overlayAvgSpeed.setText(df.format(item.getAvgSpeed() * speedAdapter.getUnitMultiplier()));
        overlayMaxSpeed.setText(df.format(item.getMaxSpeed() * speedAdapter.getUnitMultiplier()));
        overlayDateTime.setText(DateUtils.formatDateTime(getActivity(), item.getTime(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE));

        long millis = item.getDuration();
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        overlayDuration.setText("Duration: " + hms);
        ((TextView) getActivity().findViewById(R.id.txtUnit)).setText(isMetric ? Constants.KMPH : Constants.MPH);
        deleteButton.setTag(item.getId());
    }

    private final View.OnClickListener mSetDetailsOverlayClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        detailsOverlay.setVisibility(View.GONE);
        }
    };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    public boolean onBackPressed() {
        if(detailsOverlay.getVisibility() == View.VISIBLE) {
            detailsOverlay.setVisibility(View.GONE);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public android.support.v4.content.Loader<List<SpeedItem>> onCreateLoader(int id, Bundle args) {
        Log.d("SpeedHistoryFragment", "onCreateLoader");
        return new AsyncSpeedLoader(getActivity().getApplicationContext());
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<List<SpeedItem>> cursorLoader, List<SpeedItem> items) {

        Log.d("SpeedHistoryFragment", "onLoadFinished");
        speedAdapter.setItems(items);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<List<SpeedItem>> cursorLoader) {

    }

    public void onUnitChanged(boolean isMetric){
        this.isMetric = isMetric;
        if(speedAdapter != null) {
            speedAdapter.onUnitChanged(isMetric);
        }
    }

    public void onSpeedTrackingStopped() {
        getLoaderManager().restartLoader(0, null, this);
    }
}
