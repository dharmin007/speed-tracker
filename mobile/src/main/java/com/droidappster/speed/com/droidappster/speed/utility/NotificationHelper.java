package com.droidappster.speed.com.droidappster.speed.utility;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.droidappster.speed.LocationService;
import com.droidappster.speed.MainActivity;
import com.droidappster.speed.R;

/**
 * This class helps with managing notifications wearable and otherwise.
 */
public class NotificationHelper {

    private static final int NOTIFICATION_ID = 001;
    public static void showNotification(Context context, String title, String text, LocationService.State state){

        // Build intent for notification content
        Intent viewIntent = new Intent(context, MainActivity.class);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(context, 0, viewIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(viewPendingIntent)
                        .setAutoCancel(false);

        String pauseText = "Pause";
        int pauseIcon = R.drawable.ic_action_pause;
        Intent pauseIntent = new Intent(context, LocationService.class);
        if(state.equals(LocationService.State.PAUSED)){
            pauseIntent.setAction(LocationService.ACTION_START);
            pauseText = "Start";
            pauseIcon = R.drawable.ic_action_play;
        } else {
            pauseIntent.setAction(LocationService.ACTION_PAUSE);
        }
        PendingIntent pausePendingIntent = PendingIntent.getService(context, 1, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(pauseIcon, pauseText, pausePendingIntent);

        Intent stopIntent = new Intent(context, LocationService.class);
        stopIntent.setAction(LocationService.ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getService(context, 1, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(R.drawable.ic_action_stop, "Stop", stopPendingIntent);

        Intent resetIntent = new Intent(context, LocationService.class);
        resetIntent.setAction(LocationService.ACTION_RESET);
        PendingIntent resetPendingIntent = PendingIntent.getService(context, 1, resetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(R.drawable.ic_action_replay, "Reset", resetPendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        Notification notification = notificationBuilder.build();
        notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;
        notificationManagerCompat.notify(NOTIFICATION_ID, notification);
    }

    public static void clearNotification(Context context){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }
}
