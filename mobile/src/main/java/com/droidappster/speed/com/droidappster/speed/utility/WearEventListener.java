package com.droidappster.speed.com.droidappster.speed.utility;

import android.os.Bundle;

/**
 * Created by dharmin on 7/27/14.
 */
public interface WearEventListener {
    public void connected(Bundle connectionHint);
}
