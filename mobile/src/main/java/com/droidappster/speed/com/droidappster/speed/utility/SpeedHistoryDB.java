package com.droidappster.speed.com.droidappster.speed.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.droidappster.speed.SpeedItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by dharmin on 7/25/14.
 */
public class SpeedHistoryDB extends SQLiteOpenHelper {
    public static final String HISTORY_DB_NAME = "SpeedHistoryDB";
    public static final String HISTORY_TABLE_NAME = "SpeedHistoryTable";
    public static final int DB_VERSION = 2;
    public static final String ID = "_id";
    public static final String TIME = "time";
    public static final String MAX_SPEED = "max_speed";
    public static final String AVERAGE_SPEED = "avg_speed";
    public static final String DURATION = "duration";

    private Executor executorService = Executors.newSingleThreadExecutor();

    private volatile static SpeedHistoryDB instance;

    public static SpeedHistoryDB getInstance(Context context) {
        if(instance==null){
            instance = new SpeedHistoryDB(context);
        }
        return instance;
    }

    private SpeedHistoryDB(Context context) {
        super(context, HISTORY_DB_NAME, null, DB_VERSION);
    }

    public void insert(final float maxSpeed, final float avgSpeed, final long duration){
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                ContentValues values = new ContentValues();
                values.put(TIME, System.currentTimeMillis());
                values.put(MAX_SPEED, maxSpeed);
                values.put(AVERAGE_SPEED, avgSpeed);
                values.put(DURATION, duration);
                trimLRU();
                synchronized (this) {
                    getWritableDatabase().insert(HISTORY_TABLE_NAME, null, values);
                }
            }
        });
    }

    public Cursor queryAll(){
        synchronized (this){
            return getReadableDatabase().query(HISTORY_TABLE_NAME, null, null, null, null, null, TIME + " DESC");
        }
    }

    public List<SpeedItem> getAll(){
        ArrayList<SpeedItem> items = new ArrayList<SpeedItem>();
        Cursor cursor = queryAll();
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                do {
                    items.add(new SpeedItem(cursor));
                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        return items;
    }

    public long delete(int id) {
        synchronized (this) {
            return getWritableDatabase().delete(HISTORY_TABLE_NAME, ID + " = ?", new String[]{String.valueOf(id)});
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE IF NOT EXISTS "+HISTORY_TABLE_NAME+
                "(" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                TIME + " INTEGER NOT NULL," +
                MAX_SPEED + " FLOAT," +
                AVERAGE_SPEED + " FLOAT," +
                DURATION + " INTEGER" +
                ")";
        synchronized (this) {
            db.execSQL(createQuery);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        synchronized (this) {
            db.execSQL("DROP TABLE " + HISTORY_TABLE_NAME);
        }
        onCreate(db);
    }

    private void trimLRU(){
        synchronized (this){
            Cursor cursor = getReadableDatabase().query(HISTORY_TABLE_NAME, new String[]{TIME}, null, null, null, null, TIME+" DESC", "500");
            if(cursor.moveToFirst() && cursor.getCount()==500){
                cursor.moveToLast();
                long time = cursor.getLong(cursor.getColumnIndexOrThrow(TIME));
                getWritableDatabase().delete(HISTORY_TABLE_NAME, "TIME < ?", new String[]{String.valueOf(time)});
            }
        }
    }
}
