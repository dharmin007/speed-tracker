package com.droidappster.speed.com.droidappster.speed.utility;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.droidappster.speed.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dharmin on 7/27/14.
 */
public class WearHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "WearDataHelper";
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError = false;
    private WearEventListener wearEventListener;
    private ExecutorService mExecutorService = Executors.newSingleThreadExecutor();

    private WearHelper(WearEventListener wearEventListener){
        this.wearEventListener = wearEventListener;
    }

    public void onCreate(Context context){

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void onStart() {
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }

    public GoogleApiClient getGoogleApiClient(){
        return this.mGoogleApiClient;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mResolvingError = false;
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Connected to Google Api Service");
        }
        wearEventListener.connected(connectionHint);
    }

    public void onStop(){
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mResolvingError = true;
    }

    public boolean isWearableConnected(){
        return !getNodes().isEmpty();
    }

    public void sendMessage(final LocationService.State state){

        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                if(isWearableConnected()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, getNodes().iterator().next(), "/speed/action", state.name().getBytes()).await();
                    if (!result.getStatus().isSuccess()) {
                        Log.e(TAG, "ERROR: failed to send Message: " + result.getStatus());
                    }

                }
            }
        });
    }

    private Collection<String> getNodes() {
        HashSet<String> results= new HashSet<String>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : nodes.getNodes()) {
            results.add(node.getId());
        }
        return results;
    }
}
