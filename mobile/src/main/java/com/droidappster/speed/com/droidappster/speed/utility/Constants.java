package com.droidappster.speed.com.droidappster.speed.utility;

/**
 * Created by dharmin on 7/27/14.
 */
public class Constants {
    public static final String PREFS_SPEED_UNIT = "prefs.speed_unit";
    public static final String SPEED_FORMATTER = "##.#";
    public static final String MPH = "mph";
    public static final String KMPH = "kmph";

}
