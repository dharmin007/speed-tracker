package com.droidappster.speed.com.droidappster.speed.utility;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.droidappster.speed.SpeedItem;

import java.util.List;

/**
 * Created by dharmin on 9/3/14.
 */
public class AsyncSpeedLoader extends AsyncTaskLoader<List<SpeedItem>> {

    private Context mContext;
    public AsyncSpeedLoader(Context context){
        super(context);
        this.mContext = context;
    }

    @Override
    public List<SpeedItem> loadInBackground() {
        return SpeedHistoryDB.getInstance(mContext).getAll();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
