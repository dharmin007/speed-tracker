package com.droidappster.speed;

import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.droidappster.speed.com.droidappster.speed.utility.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.text.DecimalFormat;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment implements SpeedListener {
    public static final String TAG = "MainFragment";
    private TextView txtSpeed, txtMaxSpeed, txtAvgSpeed;
    private LocationService mLocationService;
    private SharedPreferences prefs;
    private boolean paused = false;
    private SpeedEventListener mUnitChangeListener;
    private LocationServiceConn locationServiceConn;
    private Button btnStart;
    private ImageButton btnReset, btnPause;
    private View btnUnitSelector, viewLine, unitSelectorView;
    private TextView kmph, mph, txtMaxSpeedUnit, txtAvgSpeedUnit;
    private DecimalFormat df = new DecimalFormat(Constants.SPEED_FORMATTER);
    private static MainFragment currentFragment;

    public MainFragment() {
        locationServiceConn = new LocationServiceConn();
    }

    @Override
    public void onAttach(Activity activity) {
        try {
            mUnitChangeListener = (SpeedEventListener) activity;
        } catch (ClassCastException e) {
            Log.e("SpeedHistoryFragment", "Activity needs to implement SpeedUnitChangeListener");
        }
        super.onAttach(activity);
    }

    private void animateToNonMetric(View v) {
        v.animate().x(viewLine.getRight() - 55).setDuration(600).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                try {
                    mLocationService.setMetric(false);
                    mph.setTextAppearance(getActivity(), R.style.UnitSelectorText);
                    kmph.setTextAppearance(getActivity(), R.style.UnitSelectorTextInactive);
                    txtAvgSpeedUnit.setText("mph");
                    txtMaxSpeedUnit.setText("mph");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    private void animateToMetric(View v) {
        v.animate().x(40).setDuration(600).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                try {
                    mLocationService.setMetric(true);
                    kmph.setTextAppearance(getActivity(), R.style.UnitSelectorText);
                    mph.setTextAppearance(getActivity(), R.style.UnitSelectorTextInactive);
                    txtAvgSpeedUnit.setText("kmph");
                    txtMaxSpeedUnit.setText("kmph");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "starting...");
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        getActivity().getApplicationContext().startService(new Intent(getActivity(), LocationService.class));
    }

    @Override
    public void onDestroy() {

        if (mLocationService != null) {
            mLocationService.unregisterListener(this);
            getActivity().getApplicationContext().unbindService(locationServiceConn);
        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        txtSpeed = (TextView) rootView.findViewById(R.id.txtSpeed);
        txtAvgSpeed = (TextView) rootView.findViewById(R.id.txtAvgSpeed);
        txtMaxSpeed = (TextView) rootView.findViewById(R.id.txtMaxSpeed);


        btnStart = (Button) rootView.findViewById(R.id.btnStart);
        btnReset = (ImageButton) rootView.findViewById(R.id.btnReset);
        btnPause = (ImageButton) rootView.findViewById(R.id.btnPause);
        btnUnitSelector = rootView.findViewById(R.id.btnUnitSelector);
        viewLine = rootView.findViewById(R.id.line);
        kmph = (TextView) rootView.findViewById(R.id.txtSpeedKmph);
        mph = (TextView) rootView.findViewById(R.id.txtSpeedMph);
        unitSelectorView = rootView.findViewById(R.id.speed_selector);
        txtMaxSpeedUnit = (TextView) rootView.findViewById(R.id.maxSpeedUnit);
        txtAvgSpeedUnit = (TextView) rootView.findViewById(R.id.avgSpeedUnit);

        btnStart.setOnClickListener(onClickListener);
        btnReset.setOnClickListener(onClickListener);
        btnPause.setOnClickListener(onClickListener);
        unitSelectorView.setOnClickListener(onClickListener);

        return rootView;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnStart:
                    if (mLocationService != null) {
                        if (!mLocationService.isRunning()) {
                            if (LocationService.State.RUNNING.equals(mLocationService.start())) {
                                onSpeedTrackingStarted();
                            }
                        } else {
                            mLocationService.stop();
                            onSpeedTrackingStopped();
                        }
                    }
                    getActivity().findViewById(R.id.layoutLockingGps).setVisibility(View.GONE);
                    break;
                case R.id.btnPause:
                    if (mLocationService != null) {
                        mLocationService.pause();
                    }
                    onSpeedTrackingPaused();
                    getActivity().findViewById(R.id.layoutLockingGps).setVisibility(View.GONE);
                    break;
                case R.id.btnReset:
                    /**
                     * TODO: Confirm reset...!
                     */
                    if (mLocationService != null) {
                        if (paused) {
                            btnPause.animate().x(btnStart.getX() - btnPause.getWidth() - 40).setDuration(300);
                        }
                        btnStart.setText("Stop");
                        paused = false;
                        mLocationService.stop();
                        mLocationService.start();
                    }
                    break;
                case R.id.speed_selector:
                    v = btnUnitSelector;
                    if (mLocationService != null) {
                        if (v.getX() < 300) {
                            animateToNonMetric(v);

                        } else {
                            animateToMetric(v);
                        }
                    }
                    break;
            }
        }
    };

    private boolean isSpeedUpdated = false;

    @Override
    public void onSpeedUpdated(Location location) {
        if (!isSpeedUpdated) {
            getActivity().findViewById(R.id.layoutLockingGps).setVisibility(View.GONE);
            isSpeedUpdated = true;
        }
        txtSpeed.setText(df.format(mLocationService.getSpeed()));
        txtAvgSpeed.setText(df.format(mLocationService.getAverageSpeed()));
        txtMaxSpeed.setText(df.format(mLocationService.getMaxSpeed()));
    }

    @Override
    public void onStart() {
        getActivity().getApplicationContext().bindService(new Intent(getActivity().getApplicationContext(), LocationService.class), locationServiceConn, Service.BIND_AUTO_CREATE);
        super.onStart();
    }

    @Override
    public void onResume() {
        currentFragment = this;
        boolean isMetric = prefs.getBoolean(Constants.PREFS_SPEED_UNIT, false);
        onUnitChanged(isMetric);
        super.onResume();
    }

    @Override
    public void onSpeedTrackingStarted() {
        btnStart.setText(R.string.stop);
        /**
         * Show the pause and reset buttons
         */
        btnPause.animate().x(btnStart.getX() - btnPause.getWidth() - 40).setDuration(300);
        if (!paused) {
            btnReset.animate().x(btnStart.getRight() + 40).setDuration(300);
        }
        getActivity().findViewById(R.id.layoutLockingGps).setVisibility(View.VISIBLE);
        paused = false;
    }

    @Override
    public void onSpeedTrackingStopped() {
        try {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnStart.setText(R.string.start);
                        /**
                         * Hide the pause and reset buttons
                         */
                        btnPause.animate().x(btnStart.getX()).setDuration(300);
                        btnReset.animate().x(btnStart.getX()).setDuration(300);
                        txtSpeed.setText("0");
                        if (mLocationService != null) {
                            mUnitChangeListener.onUnitChanged(mLocationService.isMetric());
                        }
                        mUnitChangeListener.onSpeedTrackingStopped();
                        isSpeedUpdated = false;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSpeedTrackingPaused() {
        try {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnStart.setText(R.string.start);
                        txtSpeed.setText("0");
                        /**
                         * Hide the pause button
                         */
                        paused = true;
                        btnPause.animate().x(btnStart.getX()).setDuration(300);
                        isSpeedUpdated = false;
                    }
                });
            }
        } catch (Exception e) {
            paused = true;
        }
    }

    @Override
    public void onUnitChanged(boolean isMetric) {
        if (txtMaxSpeedUnit != null) {
            txtMaxSpeedUnit.setText(isMetric ? Constants.KMPH : Constants.MPH);
        }
        if (txtAvgSpeedUnit != null) {
            txtAvgSpeedUnit.setText(isMetric ? Constants.KMPH : Constants.MPH);
        }
        if (isMetric) {
            animateToMetric(btnUnitSelector);
        }

        mUnitChangeListener.onUnitChanged(isMetric);
    }

    public interface SpeedEventListener {
        public void onUnitChanged(boolean isMetric);

        public void onSpeedTrackingStopped();
    }

    private class LocationServiceConn implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            mLocationService = ((LocationService.LocalBinder) service).getService();
            mLocationService.registerListener(MainFragment.this);
            if (mLocationService.isRunning()) {
                onSpeedTrackingStarted();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mLocationService = null;
        }
    }


    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
        /**
         * Try to resolve google play services error
         */
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(getActivity(), REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
//                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(connectionResult.getErrorCode());
            mResolvingError = true;
        }
    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            currentFragment.onDialogDismissed();
        }
    }
}
