package com.droidappster.speed;

import android.location.Location;

import com.google.android.gms.common.ConnectionResult;

/**
 * Created by dharmin on 7/25/14.
 */
public interface SpeedListener {
    public void onSpeedUpdated(Location location);
    public void onConnectionFailed(ConnectionResult connectionResult);
    public void onSpeedTrackingStarted();
    public void onSpeedTrackingStopped();
    public void onSpeedTrackingPaused();
    public void onUnitChanged(boolean metric);
}
