package com.droidappster.speed;

import android.database.Cursor;

import com.droidappster.speed.com.droidappster.speed.utility.SpeedHistoryDB;

/**
 * Created by dharmin on 10/19/14.
 */
public class SpeedItem {
    private float mMaxSpeed;
    private float mAvgSpeed;
    private long mTime;
    private long mDuration;
    private int mId;

    public SpeedItem(Cursor cursor) {
        this.mId = cursor.getInt(cursor.getColumnIndexOrThrow(SpeedHistoryDB.ID));
        this.mMaxSpeed = cursor.getFloat(cursor.getColumnIndexOrThrow(SpeedHistoryDB.MAX_SPEED));
        this.mAvgSpeed = cursor.getFloat(cursor.getColumnIndexOrThrow(SpeedHistoryDB.AVERAGE_SPEED));
        this.mTime = cursor.getLong(cursor.getColumnIndexOrThrow(SpeedHistoryDB.TIME));
        this.mDuration = cursor.getLong(cursor.getColumnIndexOrThrow(SpeedHistoryDB.DURATION));
    }

    public SpeedItem(int id, float maxSpeed, float avgSpeed, long time, long duration){
        this.mId = id;
        this.mMaxSpeed = maxSpeed;
        this.mAvgSpeed = avgSpeed;
        this.mTime = time;
        this.mDuration = duration;
    }

    public float getMaxSpeed() {
        return mMaxSpeed;
    }

    public float getAvgSpeed() {
        return mAvgSpeed;
    }

    public long getTime() {
        return mTime;
    }

    public long getDuration() {
        return mDuration;
    }

    public int getId() {
        return mId;
    }
}
