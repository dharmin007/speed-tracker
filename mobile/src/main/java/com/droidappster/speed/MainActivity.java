package com.droidappster.speed;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;


public class MainActivity extends FragmentActivity implements MainFragment.SpeedEventListener {

    private FragPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private SpeedHistoryFragment mSpeedHistoryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSpeedHistoryFragment = new SpeedHistoryFragment();
        mPagerAdapter = new FragPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(mPagerAdapter);

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        // Specify that tabs should be displayed in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Add 2 tabs, specifying the tab's text and TabListener
        actionBar.addTab(actionBar.newTab().setText(R.string.speed).setTabListener(tabListener));
        actionBar.addTab(actionBar.newTab().setText(R.string.past).setTabListener(tabListener));

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
                super.onPageSelected(position);
            }
        });

        startService(new Intent(getApplicationContext(), LocationService.class));
    }

    // Create a tab listener that is called when the user changes tabs.
    private ActionBar.TabListener tabListener = new ActionBar.TabListener() {
        @Override
        public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            mViewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

        }
    };

    @Override
    public void onBackPressed() {
        if(getActionBar().getSelectedNavigationIndex()>0){
            if(!mSpeedHistoryFragment.onBackPressed()) {
                getActionBar().setSelectedNavigationItem(0);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onUnitChanged(boolean isMetric) {
        mSpeedHistoryFragment.onUnitChanged(isMetric);
    }

    @Override
    public void onSpeedTrackingStopped() {
        mSpeedHistoryFragment.onSpeedTrackingStopped();
    }

    private class FragPagerAdapter extends FragmentPagerAdapter {

        public FragPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch(i){
                case 0:
                    return new MainFragment();
                case 1:
                    return mSpeedHistoryFragment;
            }
            return new MainFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
