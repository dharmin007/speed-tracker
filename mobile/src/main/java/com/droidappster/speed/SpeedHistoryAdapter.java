package com.droidappster.speed;

import android.content.Context;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.droidappster.speed.com.droidappster.speed.utility.Constants;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by dharmin on 8/24/14.
 */
public class SpeedHistoryAdapter extends BaseAdapter{

    private LayoutInflater inflater;
    private Context mContext;
    private DecimalFormat df;
    private float unitMultiplier;
    private List<SpeedItem> mItems;

    public SpeedHistoryAdapter(Context context, List<SpeedItem> items){
        this.inflater = LayoutInflater.from(context);
        this.mContext = context;
        df = new DecimalFormat("##.#");
        unitMultiplier = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(Constants.PREFS_SPEED_UNIT, false)
                ? LocationService.MPS_TO_KMPH : LocationService.MPS_TO_MPH;
        mItems = items;
    }

    public void setItems(List<SpeedItem> items){
        mItems = items;
        notifyDataSetChanged();
    }

    public void onUnitChanged(boolean isMetric){
        unitMultiplier = isMetric ? LocationService.MPS_TO_KMPH : LocationService.MPS_TO_MPH;
        notifyDataSetChanged();
    }

    public float getUnitMultiplier(){
        return unitMultiplier;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.speed_grid_item, null);
        }

        SpeedItem item = (SpeedItem) getItem(position);

        ((TextView)convertView.findViewById(R.id.txtDate)).setText(DateUtils.formatDateTime(mContext, item.getTime(), DateUtils.FORMAT_SHOW_DATE));

        float maxSpeed = item.getMaxSpeed() * unitMultiplier;
        ((TextView)convertView.findViewById(R.id.txtMaxSpeed)).setText(df.format(maxSpeed));

        if ((position % 2 == 0 && (position / 2) % 2 != 0) || (position % 2 != 0 && (position / 2) % 2 == 0)) {
            convertView.findViewById(R.id.relativeLayout).setBackgroundResource(R.drawable.alternate_grid_item);
        } else {
            convertView.findViewById(R.id.relativeLayout).setBackgroundResource(R.drawable.grid_item);
        }

        return convertView;
    }
}
