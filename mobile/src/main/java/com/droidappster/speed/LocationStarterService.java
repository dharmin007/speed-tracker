package com.droidappster.speed;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by dharmin on 7/27/14.
 */
public class LocationStarterService extends WearableListenerService{
    private static final String TAG = "LocationStaterService";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d(TAG, "onMessageReceived");
        Intent intent = new Intent(this, LocationService.class);
        intent.setAction(new String(messageEvent.getData()));
        startService(intent);
        super.onMessageReceived(messageEvent);
    }
}
