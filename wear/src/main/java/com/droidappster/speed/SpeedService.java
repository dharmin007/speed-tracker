package com.droidappster.speed;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SpeedService extends WearableListenerService implements WearEventListener{

    public static final String ACTION_START = "speed/START";
    public static final String ACTION_STOP = "speed/STOP";
    public static final String ACTION_PAUSE = "speed/PAUSE";
    public static final String ACTION_RESET = "speed/RESET";
    public static final String ACTION_GET_STATUS = "get/STATUS";
    private static final String TAG = "SpeedService";

    private ExecutorService mExecutorService = Executors.newSingleThreadExecutor();
    private WearDataHelper mWearDataHelper;

    @Override
    public void onCreate() {
        mWearDataHelper = new WearDataHelper(this);
        mWearDataHelper.onCreate(this);
        super.onCreate();
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        Log.d("SpeedService", messageEvent.getPath());

        Constants.State state = Constants.State.valueOf(new String(messageEvent.getData()));
        switch(state){
            case RUNNING:
            case PAUSED:
                NotificationHelper.showNotification(this, state);
                break;
            case STOPPED:
                NotificationHelper.clearNotification(this);
                break;
        }
        super.onMessageReceived(messageEvent);
    }

    private Intent mIntent;
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        if(intent != null) {
            mIntent = intent;
        }
        if(mWearDataHelper.getGoogleApiClient().isConnected()){
            mExecutorService.execute(mMessageSender);
        } else {
            mWearDataHelper.onStart();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        mWearDataHelper.onStop();
        super.onDestroy();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mExecutorService.execute(mMessageSender);
    }

    private Runnable mMessageSender = new Runnable() {
        @Override
        public void run() {

            synchronized (this) {
                final String action = mIntent.getAction();
                mIntent = null;
                NodeApi.GetConnectedNodesResult nodes =
                        Wearable.NodeApi.getConnectedNodes(mWearDataHelper.getGoogleApiClient()).await();
                if(nodes != null && !nodes.getNodes().isEmpty()) {
                    Node node = nodes.getNodes().get(0);

                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mWearDataHelper.getGoogleApiClient(), node.getId(), "/speed/action", action.getBytes()).await();
                    if (!result.getStatus().isSuccess()) {
                        Log.e(TAG, "ERROR: failed to send Message: " + result.getStatus());
                    }
                }
            }
        }
    };
}
