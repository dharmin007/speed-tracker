package com.droidappster.speed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.GridViewPager;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

import java.util.concurrent.ConcurrentHashMap;

public class MainActivity extends Activity implements DataApi.DataListener, WearEventListener, SpeedViewerFragment.SpeedChangeNotifier{

    private static final String TAG = "MainActivity";
    private WearDataHelper wearDataHelper;
    private SpeedGridPagerAdapter mPagerAdapter;
    private ConcurrentHashMap<Integer, DataApi.DataListener> mDataChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wearDataHelper = new WearDataHelper(this);
        wearDataHelper.onCreate(this);
        mDataChangeListener = new ConcurrentHashMap<Integer, DataApi.DataListener>();

        GridViewPager gridViewPager = new GridViewPager(this);
        gridViewPager.setId(View.generateViewId());
        setContentView(gridViewPager);
        mPagerAdapter = new SpeedGridPagerAdapter(getFragmentManager());
        gridViewPager.setAdapter(mPagerAdapter);

        Intent speedIntent = new Intent(this, SpeedService.class);
        speedIntent.setAction(SpeedService.ACTION_GET_STATUS);
        startService(speedIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        wearDataHelper.onStart();
    }

    @Override
    protected void onStop() {
        GoogleApiClient googleApiClient = wearDataHelper.getGoogleApiClient();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(googleApiClient, this);
        }
        wearDataHelper.onStop();
        super.onStop();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged");

        DataItem dataItem = dataEvents.get(0).getDataItem();
        if(dataItem.isDataValid() && dataItem.getUri().getPath().equals("/speed/state")){
            final boolean isRunning = DataMapItem.fromDataItem(dataItem).getDataMap().getString("state").equals(Constants.State.RUNNING.name());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(isRunning) {
                        mPagerAdapter.setRunning(true);
                    } else{
                        mPagerAdapter.setRunning(false);
                    }
                }
            });
        } else {
            for (DataApi.DataListener dataListener : mDataChangeListener.values()) {
                dataListener.onDataChanged(dataEvents);
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "onConnected "+getPackageName());
        Wearable.DataApi.addListener(wearDataHelper.getGoogleApiClient(), this);
    }

    @Override
    public void registerListener(DataApi.DataListener dataListener) {
        mDataChangeListener.put(dataListener.hashCode(), dataListener);
    }

    @Override
    public void unregisterListener(DataApi.DataListener dataListener) {
        mDataChangeListener.remove(dataListener.hashCode(), dataListener);
    }
}
