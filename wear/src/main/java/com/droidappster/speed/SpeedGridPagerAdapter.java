package com.droidappster.speed;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.wearable.view.FragmentGridPagerAdapter;

/**
 * Created by dharmin on 9/21/14.
 */
public class SpeedGridPagerAdapter extends FragmentGridPagerAdapter {

    private boolean mIsRunning = false;
    private SpeedViewerFragment mCurrentSpeedViewer;
    private SpeedViewerFragment mMaxSpeedViewer;

    public SpeedGridPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount(int row) {
        return mIsRunning ? 4 : 2;
    }

    @Override
    public Fragment getFragment(int i, int j) {
        if (getColumnCount(0) == 4) {
            switch (j){
                case 0:
                    if(mCurrentSpeedViewer == null){
                        mCurrentSpeedViewer = SpeedViewerFragment.newInstance(true);
                    }
                    return mCurrentSpeedViewer;

                case 1:
                    if (mMaxSpeedViewer == null) {
                        mMaxSpeedViewer = SpeedViewerFragment.newInstance(false);
                    }
                    return mMaxSpeedViewer;

                case 2:
                    return ActionFragment.newInstance(SpeedService.ACTION_PAUSE);
            }
        }
        if(j == getColumnCount(0)-1){
            return ActionFragment.newInstance(SpeedService.ACTION_STOP);
        }
        return ActionFragment.newInstance(SpeedService.ACTION_START);
    }

    public void setRunning(boolean isRunning){
        this.mIsRunning = isRunning;
        notifyDataSetChanged();
    }
}
