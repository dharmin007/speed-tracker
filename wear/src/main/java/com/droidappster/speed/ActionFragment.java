package com.droidappster.speed;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ActionFragment extends Fragment implements View.OnClickListener{


    public static final String PARAM_ACTION = "action";

    public ActionFragment() {
        // Required empty public constructor
    }

    public static ActionFragment newInstance(String action){
        ActionFragment actionFragment = new ActionFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_ACTION, action);
        actionFragment.setArguments(bundle);
        return actionFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_action, container, false);
        String action = getArguments().getString(PARAM_ACTION);
        TextView txtAction = (TextView) view.findViewById(R.id.txtAction);
        ImageView imgAction = (ImageView) view.findViewById(R.id.imgAction);

        if(action.equals(SpeedService.ACTION_PAUSE)){
            txtAction.setText(R.string.pause);
            imgAction.setImageResource(R.drawable.ic_action_pause);
        } else if(action.equals(SpeedService.ACTION_START)){
            txtAction.setText(R.string.start);
            imgAction.setImageResource(R.drawable.ic_action_play);
        } else if(action.equals(SpeedService.ACTION_STOP)){
            txtAction.setText(R.string.stop);
            imgAction.setImageResource(R.drawable.ic_action_stop);
        } else{
            throw new IllegalArgumentException("Invalid action provided for ActionFragment");
        }
        imgAction.setOnClickListener(this);
        imgAction.setTag(action);
        return view;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), SpeedService.class);
        intent.setAction((String) view.getTag());
        getActivity().getApplicationContext().startService(intent);

        Intent confirmationIntent = new Intent(getActivity(), ConfirmationActivity.class);
        confirmationIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
        startActivity(confirmationIntent);
    }
}
