package com.droidappster.speed;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/**
 * Created by dharmin on 9/21/14.
 */
public class NotificationHelper {

    private static final int NOTIFICATION_ID = 001;

    public static void showNotification(Context context, Constants.State state) {

        // Build intent for notification content
        Intent viewIntent = new Intent(context, MainActivity.class);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(context, 0, viewIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Tracking speed...")
                        .setContentText("Be safe!")
                        .setContentIntent(viewPendingIntent)
                        .setAutoCancel(false);

        String pauseText = "Pause";
        int pauseIcon = R.drawable.ic_action_pause;
        Intent pauseIntent = new Intent(context, SpeedService.class);
        if(state.equals(Constants.State.PAUSED)){
            pauseIntent.setAction(SpeedService.ACTION_START);
            pauseText = "Start";
            pauseIcon = R.drawable.ic_action_play;
        } else {
            pauseIntent.setAction(SpeedService.ACTION_PAUSE);
        }
        PendingIntent pausePendingIntent = PendingIntent.getService(context, 1, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(pauseIcon, pauseText, pausePendingIntent);

        Intent stopIntent = new Intent(context, SpeedService.class);
        stopIntent.setAction(SpeedService.ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getService(context, 1, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.addAction(R.drawable.ic_action_stop, "Stop", stopPendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notificationManagerCompat.notify(NOTIFICATION_ID, notification);
    }

    public static void clearNotification(Context context) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.cancelAll();
    }
}
