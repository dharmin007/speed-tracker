package com.droidappster.speed;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;


/**
 * A simple {@link Fragment} subclass to view current speed.
 *
 */
public class SpeedViewerFragment extends Fragment implements DataApi.DataListener{

    private static final String TAG = "SpeedViewerFragment";
    public static final String IS_CURRENT_SPEED = "current_speed";
    private TextView mSpeedText, mUnitText;
    private SpeedChangeNotifier mSpeedChangeNotifier = null;

    public SpeedViewerFragment() {
        // Required empty public constructor
    }

    public interface SpeedChangeNotifier {
        public void registerListener(DataApi.DataListener dataListener);
        public void unregisterListener(DataApi.DataListener dataListener);
    }

    public static SpeedViewerFragment newInstance(boolean current){
        SpeedViewerFragment fragment = new SpeedViewerFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_CURRENT_SPEED, current);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.speed_viewer, null);
        final WatchViewStub stub = (WatchViewStub) view.findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                Log.d(TAG, "onLayoutInflated");
                mSpeedText = (TextView) stub.findViewById(R.id.speed);
                mUnitText = (TextView) stub.findViewById(R.id.unit);
                boolean isCurrentSpeed = getArguments().getBoolean(IS_CURRENT_SPEED);
                if(isCurrentSpeed){
                    stub.findViewById(R.id.txtSpeedType).setVisibility(View.GONE);
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        try {
            Log.d(TAG, "onAttach");
            mSpeedChangeNotifier = (SpeedChangeNotifier) activity;
            mSpeedChangeNotifier.registerListener(this);
        } catch(ClassCastException e){
            Log.e(TAG, "Activity needs to implement SpeedChangeNotifier");
        }
        super.onAttach(activity);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        if(mSpeedChangeNotifier != null){
            mSpeedChangeNotifier.unregisterListener(this);
        }
        super.onDestroy();
    }

    @Override
    public void onDataChanged(final DataEventBuffer dataEvents) {
        for (DataEvent event : dataEvents) {
            if (event.isDataValid() && event.getType() == DataEvent.TYPE_DELETED) {
                Log.d(TAG, "DataItem deleted: " + event.getDataItem().getUri());
            } else if (event.getType() == DataEvent.TYPE_CHANGED) {
                Log.d(TAG, "DataItem changed: " + event.getDataItem().getUri());
            }

            DataMap dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
            final String speed;
            final boolean isCurrentSpeed = getArguments().getBoolean(IS_CURRENT_SPEED);
            if (isCurrentSpeed) {
                speed = dataMap.getString("speed");
            } else {
                speed = dataMap.getString("max");
            }
            final Boolean isMetric = dataMap.getBoolean("metric");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(TextUtils.isEmpty(mSpeedText.getText()) || !speed.equals("0") || isCurrentSpeed) {
                        mSpeedText.setText(speed);
                    }
                    mUnitText.setText(isMetric ? Constants.KMPH : Constants.MPH);
                }
            });
        }
    }

}
