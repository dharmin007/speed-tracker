package com.droidappster.speed;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by dharmin on 7/27/14.
 */
public class WearDataHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "WearDataHelper";
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError = false;
    private WearEventListener wearEventListener;

    public WearDataHelper(WearEventListener wearEventListener){
        this.wearEventListener = wearEventListener;
    }

    public void onCreate(Context context){

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    protected void onStart() {
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }

    public GoogleApiClient getGoogleApiClient(){
        return this.mGoogleApiClient;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mResolvingError = false;
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Connected to Google Api Service");
        }
        wearEventListener.onConnected(connectionHint);
    }

    public void onStop(){
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mResolvingError = true;
    }
}
