package com.droidappster.speed;

import android.os.Bundle;

/**
 * Created by dharmin on 7/27/14.
 */
public interface WearEventListener {
    public void onConnected(Bundle connectionHint);
}
