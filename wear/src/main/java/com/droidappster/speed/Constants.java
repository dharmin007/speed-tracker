package com.droidappster.speed;

/**
 * Created by dharmin on 9/20/14.
 */
public class Constants {
    public static final String MPH = "mph";
    public static final String KMPH = "kmph";

    public static enum State{
        RUNNING,
        PAUSED,
        STOPPED
    }
}
